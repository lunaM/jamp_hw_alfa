package com.epam.jamp.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.epam.jamp.models.Pair;
import com.epam.jamp.models.Participant;

@Repository
public class AdditionalInfoDao {
	@PersistenceContext
	EntityManager entityManager;

	public List<Object[]> getMentorsByMenteesAmount(long count) {
		String jpql = "SELECT p.mentor, COUNT(*) AS amount FROM Pair p WHERE p.groupId IN (SELECT g.id FROM Group g WHERE g.actualStart < :date AND g.plannedEnd > :date) GROUP BY mentor HAVING COUNT(mentor)>= :count";
		Query q = entityManager.createQuery(jpql);
		q.setParameter("count", count);
		q.setParameter("date", new Date());
		return q.getResultList();
	}

	public List<Participant> getMenteesWithoutMentors(String location) {
		String nativeQ = "SELECT pt.id, pt.phase_id, pt.email, pt.role, pt.status FROM participant pt JOIN phase p ON p.id = pt.phase_id WHERE pt.email NOT IN (SELECT mentee FROM pair WHERE pair.group_id = p.group_id) AND role = 1 AND p.mp_id IN (SELECT id FROM mentorshipprogram WHERE mentorshipprogram.office_location LIKE ?)";
		Query q = entityManager.createNativeQuery(nativeQ);
		q.setParameter(1, location);
		return (List<Participant>) q.getResultList();
	}

	public List<Object[]> getMenteesOverallTime(Integer start, Integer amount) {
		String jpql = "SELECT p.mentee, SUM(DATEDIFF(g.actualEnd, g.actualStart)) AS s FROM com.epam.jamp.models.Pair p JOIN com.epam.jamp.models.Group g ON p.groupId = g.id GROUP BY p.mentee ORDER BY s DESC";
		Query q = entityManager.createQuery(jpql).setMaxResults(amount).setFirstResult(start * amount);
		return (List<Object[]>) q.getResultList();
	}

	public List<Object[]> getParticipantStatistic() {
		String nativeQ = "SELECT COUNT(DISTINCT pt.email), COUNT(DISTINCT m.name), m.office_location FROM participant pt, phase p, mentorshipprogram m WHERE p.id = pt.phase_id AND p.mp_id=m.id GROUP BY m.office_location";
		Query q = entityManager.createNativeQuery(nativeQ);
		return (List<Object[]>) q.getResultList();
	}

	public List<Object[]> getCompletedParticipants(String start, String end) {
		String nativeQ = "SELECT COUNT(DISTINCT pt.email) AS e, m.name AS mn, p.name AS pn FROM participant AS pt, phase AS p, mentorshipprogram AS m WHERE p.id = pt.phase_id AND p.mp_id=m.id AND m.end BETWEEN ? AND ? GROUP BY p.id";
		Query q = entityManager.createNativeQuery(nativeQ);
		q.setParameter(1, start);
		q.setParameter(2, end);
		return (List<Object[]>) q.getResultList();
	}
}
