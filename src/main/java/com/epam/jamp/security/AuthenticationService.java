package com.epam.jamp.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.epam.jamp.models.People;
import com.epam.jamp.models.UserActivity;
import com.epam.jamp.models.UserActivityType;
import com.epam.jamp.services.PeopleService;
import com.epam.jamp.services.UserActivityService;
import com.epam.jamp.utils.UserActivityCacheManager;;

@Component
@Service
public class AuthenticationService implements UserDetailsService {

	@Autowired
	private PeopleService peopleService;
	@Autowired
	private UserActivityService activityService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		People userInfo = peopleService.getPersonById(username);
		if (userInfo == null) {
			UserActivity activity = new UserActivity(UserActivityType.LOGIN, 
					new Date(), false, username, new Long(0));
			activityService.sendUserActivity(activity);
			throw new UsernameNotFoundException("User " + username + " was not found in the database");
		}

		List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
		GrantedAuthority authority = new SimpleGrantedAuthority(userInfo.getRole().toString());
		grantList.add(authority);
		UserDetails userDetails = (UserDetails) new User(userInfo.getEmail(), userInfo.getPassword(), grantList);
		activityService.sendUserActivity(new UserActivity(UserActivityType.LOGIN, 
				new Date(), true, userInfo.getEmail(), new Long(0)));
		return userDetails;
	}
}
