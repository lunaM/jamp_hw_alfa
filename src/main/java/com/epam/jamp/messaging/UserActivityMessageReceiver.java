package com.epam.jamp.messaging;

import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import com.epam.jamp.models.UserActivity;

@Component
public class UserActivityMessageReceiver {
	static final Logger LOG = LoggerFactory.getLogger(UserActivityMessageReceiver.class);

	private static final String ACTIVITY_RESPONSE_QUEUE = "activity-response-queue";
	
	
	
	
	@JmsListener(destination = ACTIVITY_RESPONSE_QUEUE)
	public void receiveMessage(final Message<UserActivity> message) throws JMSException {
		LOG.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
		MessageHeaders headers =  message.getHeaders();
		LOG.info("Application : headers received : {}", headers);
		
		UserActivity response = message.getPayload();
		LOG.info("Application : response received : {}",response);
		
		
		LOG.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
	}
}
