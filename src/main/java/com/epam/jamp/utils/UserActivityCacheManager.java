package com.epam.jamp.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.jamp.models.UserActivity;

@Component
public class UserActivityCacheManager {

	@Autowired
	private CountableCache<UserActivity> countableCache;
	@Autowired
	private EmailSender emailSender;
	
	public void setTimeToLive(long timeToLive){
		countableCache.setTimeToLive(timeToLive);
	}

	public void saveActivity(UserActivity activity) {
		countableCache.put(activity);
	}

	public boolean isSuspicious(UserActivity activity, long count) {
		return count < countableCache.getAmountOfItem(activity);
	}

	public void sendNotification(UserActivity activity, String to) {
		emailSender.sendMail(to, "User activity failure", "User "+activity.getUserId()+" exceeded the limit of"+activity.getUserActivity());
	}

	public void checkSaveAndNotificate(UserActivity activity, long i, String adminMail) {
		saveActivity(activity);
		if(isSuspicious(activity, i)){
			sendNotification(activity, adminMail);
		}
		
	}
}
