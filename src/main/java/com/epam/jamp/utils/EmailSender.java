package com.epam.jamp.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

@Component
public class EmailSender {
	
	@Autowired
	public EmailSender(JavaMailSenderImpl javaMailSenderImpl) {
		this.javaMailSenderImpl = javaMailSenderImpl;
	}
	
	private JavaMailSenderImpl javaMailSenderImpl;
	
	public void sendMail(String to, String subject, String msg) {
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(to);
		email.setSubject(subject);
		email.setText(msg);
		javaMailSenderImpl.send(email);
	}
}
