package com.epam.jamp.utils;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.epam.jamp.models.Similar;

@Component
public class CountableCache<T extends Similar<T>> {

	private ConcurrentHashMap<Long, T> map;
	private Long timeToLive;

	public CountableCache(Long timeToLive) {
		map = new ConcurrentHashMap<>();
		this.timeToLive = timeToLive;
	}
	
	public CountableCache() {
		map = new ConcurrentHashMap<>();
	}

	public Long getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(Long timeToLive) {
		this.timeToLive = timeToLive;
	}

	private void cleanOld() {
		Iterator<Entry<Long, T>> iterator = map.entrySet().iterator();
		Entry<Long, T> current;
		Long currentTime = System.currentTimeMillis();
		while (iterator.hasNext()) {
			current = iterator.next();
			if (current.getKey() + timeToLive - currentTime <= 0) {
				iterator.remove();
			}
		}
	}

	public void put(T obj) {
		cleanOld();
		map.put(System.currentTimeMillis(), obj);
	}

	public long getAmountOfItem(T obj) {
		cleanOld();
		if (!map.containsValue(obj)) {
			return 0;
		}
		long count = map.values().stream().filter((t)->t.isSimilar(obj)).count();
		return count;
	}

	public ConcurrentHashMap<Long, T> getMap() {
		return map;
	}

	public void setMap(ConcurrentHashMap<Long, T> map) {
		this.map = map;
	}

}
