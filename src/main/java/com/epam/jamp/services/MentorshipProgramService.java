package com.epam.jamp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jamp.aop.ModifyModel;
import com.epam.jamp.aop.SaveToDB;
import com.epam.jamp.dao.MentorshipProgramDao;
import com.epam.jamp.models.MentorshipProgram;

@Component
public class MentorshipProgramService {

	@Autowired
	private MentorshipProgramDao mpDao;

	@Transactional
	public List<MentorshipProgram> getAllMentorshipPrograms() {
		return mpDao.getAllMentorshipPrograms();
	}

	@SaveToDB
	@ModifyModel
	@Transactional
	public void addMentorshipProgram(MentorshipProgram mp) {
		mpDao.addMentorshipProgram(mp);
	}

	@Transactional
	public MentorshipProgram getMentorshipProgram(Long mpId) {
		return mpDao.getMentorshipProgram(mpId);
	}

}
