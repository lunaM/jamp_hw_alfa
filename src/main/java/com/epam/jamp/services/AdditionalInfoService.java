package com.epam.jamp.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jamp.aop.ModifyPhase;
import com.epam.jamp.aop.ModifyModel;
import com.epam.jamp.aop.SaveToDB;
import com.epam.jamp.dao.AdditionalInfoDao;
import com.epam.jamp.dao.MentorshipPhasesDao;
import com.epam.jamp.models.Group;
import com.epam.jamp.models.Lecture;
import com.epam.jamp.models.Pair;
import com.epam.jamp.models.Participant;
import com.epam.jamp.models.Phase;
import com.epam.jamp.models.Role;

@Component
public class AdditionalInfoService {

	@Autowired
	private AdditionalInfoDao aiDao;

	public List<Object[]> getMentorsByMenteesAmount(int count) {
		return aiDao.getMentorsByMenteesAmount(count);

	}

	public List<Participant> getMenteesWithoutMentors(String location) {
		return aiDao.getMenteesWithoutMentors(location);
	}

	public List<Object[]> getMenteesOverallTime(Integer start, Integer amount) {
		return aiDao.getMenteesOverallTime(start, amount);
	}

	public List<Object[]> getParticipantStatistic() {
		return aiDao.getParticipantStatistic();
	}

	public List<Object[]> getCompletedParticipants(String start, String end) {
		return aiDao.getCompletedParticipants(start, end);
	}

}