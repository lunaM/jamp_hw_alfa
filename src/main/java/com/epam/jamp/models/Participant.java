package com.epam.jamp.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

//@Embeddable
@Entity
public class Participant {
	@Id
	private Long id;
	@OneToOne
	@JoinColumn(name = "email")
	private People person;
	@Enumerated(EnumType.ORDINAL)
	private Role role;
	@Enumerated(EnumType.ORDINAL)
	private ParticipantStatus status;
	@ManyToOne
	@JoinColumn(name = "phase_id")
	private Phase phase;

	@Override
	public String toString() {
		return "Participant [id=" + id + ", person=" + person + ", role=" + role + ", status=" + status + ", phase="
				+ phase + "]";
	}

	public Phase getPhase() {
		return phase;
	}

	public void setPhase(Phase phase) {
		this.phase = phase;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Participant() {
	}

	public Participant(People person, Role role, ParticipantStatus status) {
		this.person = person;
		this.role = role;
		this.status = status;
	}

	public People getPerson() {
		return person;
	}

	public void setPerson(People person) {
		this.person = person;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public ParticipantStatus getStatus() {
		return status;
	}

	public void setStatus(ParticipantStatus status) {
		this.status = status;
	}

}
