package com.epam.jamp.models;

public enum UserRole {
	USER, ADMINISTRATOR;
}
