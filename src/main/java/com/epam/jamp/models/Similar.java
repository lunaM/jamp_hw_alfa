package com.epam.jamp.models;

public interface Similar <T>{
	boolean isSimilar(T t);
}
