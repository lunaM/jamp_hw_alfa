package com.epam.jamp.aop;

import java.util.Date;
import java.util.UUID;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.jamp.models.WatchedModel;
import com.epam.jamp.services.MentorshipPhasesService;

@Component
@Aspect
public class DataManagementAspect {
	@Autowired
	private MentorshipPhasesService mps;
	
	// adding creation time or modification time for methods with merge
	@Around("execution(* com.epam.jamp.*.*.add*(..)) && @annotation(SaveToDB) && args(model,id)")
	public void addCreationInfo(ProceedingJoinPoint pjp, WatchedModel model, Long id) throws Throwable {
		if (model.getCreatedDate() == null) {
			model.setCreatedDate(new Date());
			model.setCreatedBy(UUID.randomUUID().toString());
			pjp.proceed(new Object[] { model, id });
		} else {
			addModifyModelInfo(pjp, model,id);
		}
	}
	
	// adding modification time
	@Around("execution(* com.epam.jamp.*.*.*(..)) && @annotation(ModifyModel) && args(model,id)")
	public void addModifyModelInfo(ProceedingJoinPoint pjp, WatchedModel model, Long id) throws Throwable {
		model.setLastModified(new Date());
		model.setModifiedBy(UUID.randomUUID().toString());
		pjp.proceed(new Object[] { model,id });
	}

    @After("execution(public * *(..)) && @annotation(ModifyPhase) && args(..,phaseId)")
    public void addModifyPhaseInfo(Long phaseId) throws Throwable {
		System.out.println("!!!!!!!!!!!!! - ");
		mps.setModifyPhaseDate(phaseId, UUID.randomUUID().toString());
	}
	
    @After("execution(public void addMProgPhase(String, String)) && args(mpId,name)")
    public void addCreationPhaseInfo(String mpId, String name) throws Throwable {
		mps.setCreationPhaseDate(mpId,name, UUID.randomUUID().toString());
	}
	
}
